from datetime import datetime
from functions import *
from querys import *
import json
import os

# %% PARAMENTROS DE CONEXÕES AOS BANCO DE DADOS
curr_path = os.path.dirname(os.path.abspath(__file__))
with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
    parameters = json.load(f)

with open(os.path.join(curr_path, 'pass.json'), 'r') as f:
    parameters = json.load(f)
    user = parameters['db_dw.db']['user']
    psw = parameters['db_dw.db']['pass']
    host = parameters['db_dw.db']['host']

# CONEXÃO DO BANCO MYSQL ONDE ESTA O DW
try:
    engine_dw = sqlalchemy.create_engine(
        'mysql+pymysql://{}:{}@{}:3306/prd_db_provedor'.format(user, psw, host),
        echo=False, poolclass=NullPool)
except Exception as e:
    print(e)
    sys.exit()

# 1 ler a tabela com os autorizadores
with cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
                                         parameters['prdme.db']['pass'],
                                         parameters['prdme.db']['host'])) as connection:

    df_autorizadores = pd.read_sql(sql_autorizadores, connection)

# 2 deletando os resistro para atualizar a tabela
with engine_dw.connect() as con:
    result = con.execute('delete from guias_biometria where true')

# 3 fazer um loop para pegar todas as guias cadastradas pelos autorizadores
for i, r in df_autorizadores.iterrows():
    print(i)
    sql = sql_guias.format(r['DATA'],r['CD_AUTORIZADOR'])
    print(sql)
    with cx_Oracle.connect('{}/{}@{}'.format(parameters['prdme.db']['user'],
                                             parameters['prdme.db']['pass'],
                                             parameters['prdme.db']['host'])) as connection:

        df_guias = pd.read_sql(sql, connection)

    df_guias.to_sql('guias_biometria',con=engine_dw, if_exists='append', index=False)

data_execucao = datetime.today().strftime('%d/%m/%Y %H:%M:00')
data = datetime.strptime(data_execucao,'%d/%m/%Y %H:%M:00')

with engine_dw.connect() as con:
    con.execute('''DELETE FROM historico_execucao H WHERE H.script = 'Painel_biometria' ''')
    con.execute('''INSERT INTO historico_execucao (data_execucao, script) VALUES ('{}', 'Painel_biometria')'''.format(data))











