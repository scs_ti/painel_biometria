sql_autorizadores = '''
select a.CD_AUTORIZADOR,
       to_char(a.DT_INICIO,'DD/MM/YYYY') as data
       from dbaps.v_aut_web_biometria a
'''


sql_guias = '''
SELECT trunc(g.DT_EMISSAO) as DATA,
       g.CD_AUTORIZADOR ,
       --aut.NM_AUTORIZADOR || ' | DATA INICIO: {0}' as NM_AUTORIZADOR,       
        case when g.CD_AUTORIZADOR in (3140,3168,3167,3166,3142,3139,3141)
          then 'AMBULATORIO - CLINICA TAUBATE'|| ' | DATA INICIO: {0}'
           when aut.NM_AUTORIZADOR like '%SABIN%' THEN 'SABIN  | DATA INICIO: {0}'
           else aut.NM_AUTORIZADOR || ' | DATA INICIO: {0}' end as NM_AUTORIZADOR,
       
       M.CD_MOTIVO_CONTIGENCIAL,
       M.DS_MOTIVO_CONTIGENCIAL,
       pex.CD_PRESTADOR AS CD_PRESTADOR_EXEC,
       pex.NM_PRESTADOR AS PRESTADOR_EXECUTOR,
       G.CD_PRESTADOR AS CD_PRESTADOR_END,
       G.DS_ENDERECO,
       count(*) as qtd
        FROM DBAPS.GUIA G
            inner join dbaps.AUTORIZADOR aut on aut.CD_AUTORIZADOR = g.CD_AUTORIZADOR
            inner join dbaps.PRESTADOR pex on pex.CD_PRESTADOR = g.CD_PRESTADOR_EXECUTOR

LEFT JOIN DBAPS.MOTIVO_CONTIGENCIAL M ON M.CD_MOTIVO_CONTIGENCIAL = G.CD_MOTIVO_CONTIGENCIAL
WHERE G.DT_EMISSAO >= TO_DATE('{0}', 'DD/MM/YYYY')
AND G.CD_AUTORIZADOR =  {1}
GROUP BY trunc(g.DT_EMISSAO),
  g.CD_AUTORIZADOR,
  case when g.CD_AUTORIZADOR in (3140,3168,3167,3166,3142,3139,3141)
          then 'AMBULATORIO - CLINICA TAUBATE'|| ' | DATA INICIO: {0}'
           when aut.NM_AUTORIZADOR like '%SABIN%' THEN 'SABIN  | DATA INICIO: {0}'
           else aut.NM_AUTORIZADOR || ' | DATA INICIO: {0}' end,
  M.CD_MOTIVO_CONTIGENCIAL,
  M.DS_MOTIVO_CONTIGENCIAL,
  pex.CD_PRESTADOR,
  pex.NM_PRESTADOR,
  G.CD_PRESTADOR,
  G.DS_ENDERECO
'''
